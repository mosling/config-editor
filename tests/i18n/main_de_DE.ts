<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>TestMainWindow</name>
    <message>
        <location filename="main_window.py" line="19"/>
        <source>Edit Configuration</source>
        <translation>Bearbeite Konfiguration</translation>
    </message>
    <message>
        <location filename="main_window.py" line="20"/>
        <source>Slider Value</source>
        <translation>Schieberegler Wert</translation>
    </message>
</context>
</TS>
