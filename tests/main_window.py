import logging
import sys
from os import path

from PySide6.QtCore import QSettings, QLocale
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QMainWindow, QPushButton, QVBoxLayout, QWidget, QApplication, QLabel

from ExampleConfiguration import ExampleConfiguration
from pydantic_config_ui import ConfigurationEditor, add_default_translator, add_translator

logger = logging.getLogger(__name__)


class TestMainWindow(QMainWindow):
    def __init__(self, settings: QSettings):
        super().__init__()
        self.config = ExampleConfiguration()
        self.settings = settings
        button_config = QPushButton(self.tr("Edit Configuration"))
        self.label = QLabel(self.tr("Slider Value"))
        button_config.clicked.connect(self.button_config_click)

        layout = QVBoxLayout()
        layout.addWidget(button_config)
        layout.addWidget(self.label)

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

    def button_config_click(self):
        logo = QPixmap("small_lake.jpg").scaledToWidth(200)
        title = """<html>
        <h1>Example Configuration</h1>
        <br/><b>Mosling Solutions</b>
        </html>
        """
        self.config.name = "Direct Edit Configuration"
        configuration_editor = ConfigurationEditor(ExampleConfiguration, self.config,
                                                   "",
                                                   title, logo,
                                                   self.settings, self)
        configuration_editor.show()
        configuration_editor.exec()
        self.label.setText(str(self.config.percent))
        logger.info(self.config.name)


def main():
    app = QApplication(sys.argv)
    settings = QSettings("Mosling", "Configuration Editor Test")
    logger.info(f"Settings file: {settings.fileName()}")

    QLocale.setDefault(QLocale(QLocale.Language.English, QLocale.Country.UnitedStates))
    # QLocale.setDefault(QLocale(QLocale.Language.German, QLocale.Country.Germany))
    add_default_translator(app)
    i18n_folder = path.abspath(path.join(path.dirname(__file__), 'i18n'))
    add_translator(app, "main_", i18n_folder)

    window = TestMainWindow(settings)
    window.show()
    ret_value = app.exec()
    sys.exit(ret_value)


if __name__ == "__main__":
    logging.basicConfig(encoding='utf-8',
                        level=logging.INFO,
                        format="%(levelname)s | %(module)s | %(message)s")

    main()
