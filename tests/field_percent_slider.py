from PySide6.QtGui import Qt
from PySide6.QtWidgets import QWidget, QSlider, QSizePolicy

from pydantic_config_ui.field_input import FieldInput


class FieldPercentSlider(FieldInput):

    def __init__(self, name, info):
        super().__init__(name, info)

    def create_widget(self, parent) -> QWidget:
        # using the self.field variable to store the Qt widget
        self.field = QSlider(Qt.Orientation.Horizontal, parent)
        self.field.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)
        self.field.setMinimum(self.info.get("ui_min", 0))
        self.field.setMaximum(self.info.get("ui_max", 100))
        self.add_tooltip()
        return self.field

    def set_value(self, value):
        self.field.setValue(value)

    def get_value(self):
        return self.field.value()
