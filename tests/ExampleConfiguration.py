import logging
from typing import Optional, List, Annotated

from pydantic import BaseModel, Field, validator, AfterValidator

from field_percent_slider import FieldPercentSlider

logger = logging.getLogger(__name__)


def check_alphanumeric(value: str) -> str:
    assert value.replace(" ", "").isalnum(), 'must be alphanumeric'
    return value


def check_range(value: int) -> int:
    assert 0 < value < 150, "number must between 0 and 150"
    return value


NameString = Annotated[str, AfterValidator(check_alphanumeric)]

InRange = Annotated[int, AfterValidator(check_range)]


class ExampleConfiguration(BaseModel):
    """
    This is a test pydantic class to show the usage of the additional parameters used to
    generate the dialog.
    """
    number: InRange = Field(title="Numeric", description="input number between 0 and 150", default=42)
    name: NameString = Field(title="Name Field", description="input a name", default="")
    percent: int = Field(title="Percent Field",
                         description="Select you luck in percent",
                         ui_type="slider", default=50)
    boolean_1: Optional[bool] = Field(default=False,
                                      ui_group="Advanced", description="check if you mean it is true")
    boolean_2: Optional[bool] = Field(default=False,
                                      ui_group="Advanced", description="check if you mean it is true")
    boolean_3: Optional[bool] = Field(default=False,
                                      ui_group="Advanced", description="check if you mean it is true")

    data: str = Field(default="",
                      description="select file contains some data using json",
                      ui_type="file", ui_pattern="*.json")
    output: str = Field(default="",
                        description="select folder to store the results",
                        ui_type="folder")
    array: Optional[List[str]] = Field(ui_group="Advanced", default=[])

    default_password: str = Field(default="changeMe", ui_type="hidden")

    class Config:
        validate_assignment = True

    @staticmethod
    def get_configuration_field(name: str, field):
        if field.get("type", "") == "integer" and field.get("ui_type", "") == "slider":
            logger.info("add slider")
            return FieldPercentSlider(name, field)

        return None
