import logging
import sys
import time

from PySide6 import QtCore
from PySide6.QtCore import QSettings, QLocale
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QApplication

from ExampleConfiguration import ExampleConfiguration
from pydantic_config_ui import MainConfiguration, add_default_translator, ConfigurationEditor

logger = logging.getLogger(__name__)


def show_configuration(configuration: ExampleConfiguration) -> None:
    """
    This is the main application method called with the current configuration object.
    :param configuration: the configuration object
    :return: nothing
    """
    schema = configuration.model_json_schema()
    logger.debug("example debug message")
    logger.info("example info message")
    logger.warning("example warning message")
    logger.error("example error message")

    for name, field_property in schema["properties"].items():
        logger.info(f"{name}={getattr(configuration, name)}")
        if QtCore.QThread.currentThread().isInterruptionRequested():
            # if interrupt requested, we can stop
            return
        time.sleep(1)


def main_simple() -> int:
    QApplication(sys.argv)
    config = ExampleConfiguration()
    configuration_editor = ConfigurationEditor(ExampleConfiguration, model_data=config)
    configuration_editor.show()
    rv = configuration_editor.exec()
    logger.info(f"{rv = }")
    logger.info(config.name)
    return rv


def main() -> int:
    app = QApplication(sys.argv)
    settings = QSettings("Mosling", "Configuration-Editor-Test")
    logger.info(f"Settings file: {settings.fileName()}")

    QLocale.setDefault(QLocale(QLocale.Language.German, QLocale.Country.Germany))
    add_default_translator(app)

    logo = QPixmap("small_lake.jpg").scaledToWidth(200)
    title = """<html>
    <h1>Example Configuration</h1>
    <br/><b>Mosling Solutions</b>
    </html>
    """
    window = MainConfiguration(
        ExampleConfiguration,
        "",
        show_configuration,
        title,
        logo,
        settings
    )
    window.setWindowTitle("Configuration Test")
    window.show()
    window.button_config_click()
    ret_value = app.exec()
    return ret_value


if __name__ == "__main__":
    logging.basicConfig(encoding='utf-8',
                        level=logging.INFO,
                        format="%(levelname)s | %(module)s | %(message)s")

    sys.exit(main())
