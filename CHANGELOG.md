## 0.3.1 (2023-05-02)

### fixed (1 change)

- [The geometry isn't stored in settings](mosling/config-editor@580695e91a8e51fd087e57198812a66313f282b8)

## 0.3.0 (2023-04-29)

### added (2 changes)

- [message box to ask user if nnon exiting files should be removed](mosling/config-editor@4977dc7e20ee055d29c85b13db73a27fcf62dc29) ([merge request](mosling/config-editor!2))
- [Add RecentFiles Combobox](mosling/config-editor@0282d4bd917b38c9fb26d8631dd198e0a0cd1960) ([merge request](mosling/config-editor!2))

## 0.0.4 (2023-04-27)

### added (4 changes)

- [Save geometry to Qt Settings](mosling/config-editor@9f2080c5bf93fbf95ae35550fbf40eb762ff9393)
- [Add Thread and last folder name](mosling/config-editor@11c2211c495f5733b51961fefee8ffd0a8155870)
- [Python Package to publish on PyPI](mosling/config-editor@705654a2f5c282f4cf7c13d4bc374aa8d579be0c)
- [Add Save Button, make file selectable](mosling/config-editor@4c475b43d0d785a99ead506678c149b6186ef39a)

## 0.0.3 (2022-12-26)

### Added (1 change)

- [Simple Main Window](mosling/config-editor@8e891da030fcac360903e5998f04793db94b4ae1)

### Changed (2 changes)

- [Rename MainWindow to MainConfiguration](mosling/config-editor@e3183095f08b4142946cfd9c039e011f016001e0)
- [Implement Tab Order](mosling/config-editor@15f3082dcff155521240605c4930b2f65eb36faf)
