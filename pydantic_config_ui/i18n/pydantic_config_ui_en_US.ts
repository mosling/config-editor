<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en_US">
<context>
    <name>ConfigBaseDialog</name>
    <message>
        <location filename="../config_dialog.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="53"/>
        <source>Title for the Configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="73"/>
        <source>Optional Logo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="87"/>
        <source>Arguments</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="158"/>
        <source>Save As</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="165"/>
        <source>Load</source>
        <translation>Load</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="185"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="192"/>
        <source>Ok</source>
        <translation>Ready</translation>
    </message>
</context>
<context>
    <name>ConfigurationEditor</name>
    <message>
        <location filename="../configuration_editor.py" line="120"/>
        <source>remove selected entry</source>
        <translation>delete selected entry</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="151"/>
        <source>configuration file &apos;{0}&apos; not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="162"/>
        <source>Configuration contains Errors</source>
        <translation>Configuration contains mistakes</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="163"/>
        <source>Please correct before loading again.</source>
        <translation>Please correct errors before loading again.</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="213"/>
        <source>Save Configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="224"/>
        <source>Read Configuration</source>
        <translation>Load Configuration</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="252"/>
        <source>Missing Configuration</source>
        <translation>Missing Configuration</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="253"/>
        <source>Remove &apos;{0}&apos; from recent list?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FieldFileSelect</name>
    <message>
        <location filename="../field_file_select.py" line="46"/>
        <source>Select File</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FieldFolderSelect</name>
    <message>
        <location filename="../field_folder_select.py" line="17"/>
        <source>select a folder</source>
        <translation>select a folder from your system</translation>
    </message>
    <message>
        <location filename="../field_folder_select.py" line="26"/>
        <source>Select Folder</source>
        <translation>Select Folder</translation>
    </message>
</context>
<context>
    <name>MainConfiguration</name>
    <message>
        <location filename="../configuration_exec.py" line="81"/>
        <source>
        &lt;html&gt;
        &lt;h2&gt;Setup Configuration&lt;/h2&gt;
        &lt;/html&gt;
        </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="90"/>
        <source>Clean Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="91"/>
        <source>Edit Configuration</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="92"/>
        <source>Execute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="93"/>
        <source>Finish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="151"/>
        <source>config edit canceled -- no execution possible</source>
        <translation>edit canceled -- nothing will be executed</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="168"/>
        <source>Process is running.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="169"/>
        <source>Really stop the process?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="171"/>
        <source>Yes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="172"/>
        <source>No</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="177"/>
        <source>request thread interruption</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MyLongThread</name>
    <message>
        <location filename="../configuration_exec.py" line="199"/>
        <source>Missing configuration</source>
        <translation>without configuration we do nothing</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="202"/>
        <source>Ready</source>
        <translation></translation>
    </message>
</context>
</TS>
