<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ConfigBaseDialog</name>
    <message>
        <location filename="../config_dialog.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="53"/>
        <source>Title for the Configuration</source>
        <translation>Name der Konfiguration</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="73"/>
        <source>Optional Logo</source>
        <translation>Logo</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="87"/>
        <source>Arguments</source>
        <translation>Argumente</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="158"/>
        <source>Save As</source>
        <translation>Speichern als</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="165"/>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="185"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../config_dialog.ui" line="192"/>
        <source>Ok</source>
        <translation>Fertig</translation>
    </message>
</context>
<context>
    <name>ConfigurationEditor</name>
    <message>
        <location filename="../configuration_editor.py" line="120"/>
        <source>remove selected entry</source>
        <translation>entferne ausgewählten Eintrag</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="151"/>
        <source>configuration file &apos;{0}&apos; not found</source>
        <translation>Konfigurationsdatei &apos;{0}&apos; ist nicht vorhanden</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="162"/>
        <source>Configuration contains Errors</source>
        <translation>Konfiguration enthält Fehler</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="163"/>
        <source>Please correct before loading again.</source>
        <translation>Fehler müssen korrigiert werden.</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="213"/>
        <source>Save Configuration</source>
        <translation>Speichere Konfiguration</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="224"/>
        <source>Read Configuration</source>
        <translation>Lade Konfiguration</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="252"/>
        <source>Missing Configuration</source>
        <translation>Fehlende Konfiguration</translation>
    </message>
    <message>
        <location filename="../configuration_editor.py" line="253"/>
        <source>Remove &apos;{0}&apos; from recent list?</source>
        <translation>Soll &apos;{0}&apos; entfernt werden?</translation>
    </message>
</context>
<context>
    <name>FieldFileSelect</name>
    <message>
        <location filename="../field_file_select.py" line="46"/>
        <source>Select File</source>
        <translation>Datei</translation>
    </message>
</context>
<context>
    <name>FieldFolderSelect</name>
    <message>
        <location filename="../field_folder_select.py" line="17"/>
        <source>select a folder</source>
        <translation>Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../field_folder_select.py" line="26"/>
        <source>Select Folder</source>
        <translation>Auswahl eines Verzeichnisses</translation>
    </message>
</context>
<context>
    <name>MainConfiguration</name>
    <message>
        <location filename="../configuration_exec.py" line="81"/>
        <source>
        &lt;html&gt;
        &lt;h2&gt;Setup Configuration&lt;/h2&gt;
        &lt;/html&gt;
        </source>
        <translation>
        &lt;html&gt;
        &lt;h2&gt;Bearbeite Konfiguration&lt;/h2&gt;
        &lt;/html&gt;
        </translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="90"/>
        <source>Clean Log</source>
        <translation>Lösche Meldungen</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="91"/>
        <source>Edit Configuration</source>
        <translation>Bearbeite Konfiguration</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="92"/>
        <source>Execute</source>
        <translation>Ausführen</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="93"/>
        <source>Finish</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="151"/>
        <source>config edit canceled -- no execution possible</source>
        <translation>Konfiguration-bearbeiten wurde abgebrochen -- keine Ausführung möglich</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="168"/>
        <source>Process is running.</source>
        <translation>Bearbeitung ist noch nicht beendet.</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="169"/>
        <source>Really stop the process?</source>
        <translation>Wirklich abbrechen?</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="171"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="172"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="177"/>
        <source>request thread interruption</source>
        <translation>Verarbeitung wird abgebrochen</translation>
    </message>
</context>
<context>
    <name>MyLongThread</name>
    <message>
        <location filename="../configuration_exec.py" line="199"/>
        <source>Missing configuration</source>
        <translation>Keine Konfiguration geladen</translation>
    </message>
    <message>
        <location filename="../configuration_exec.py" line="202"/>
        <source>Ready</source>
        <translation>Fertig</translation>
    </message>
</context>
</TS>
